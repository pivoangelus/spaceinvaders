//
//  GameOverScene.swift
//  SKInvaders
//
//  Created by Justin Andros on 2/29/16.
//  Copyright © 2016 Justin Andros. All rights reserved.
//

import UIKit
import SpriteKit

class GameOverScene: SKScene {
    
    // MARK: Properities
    
    var contentCreated = false
    
    // MARK: Object Lifecycle Management
    
    // MARK: Scene
    
    override func didMoveToView(view: SKView) {
        if !self.contentCreated {
            self.createContent()
            self.contentCreated = true
        }
    }
    
    func createContent() {
        let gameOverLabel = SKLabelNode(fontNamed: "Courier")
        gameOverLabel.fontSize = 50
        gameOverLabel.fontColor = SKColor.whiteColor()
        gameOverLabel.text = "Game Over!"
        gameOverLabel.position = CGPoint(x: self.size.width / 2, y: 2.0 / 3.0 * self.size.height)
        addChild(gameOverLabel)
        
        let tapLabel = SKLabelNode(fontNamed: "Courier")
        tapLabel.fontSize = 25
        tapLabel.fontColor = SKColor.whiteColor()
        tapLabel.text = "(Tap to Play Again)"
        tapLabel.position = CGPoint(x: self.size.width / 2, y: gameOverLabel.frame.origin.y - gameOverLabel.frame.size.height - 40)
        addChild(tapLabel)
        
        backgroundColor = SKColor.blackColor()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let gameScene = GameScene(size: self.size)
        gameScene.scaleMode = .AspectFill
        self.view?.presentScene(gameScene, transition: SKTransition.doorsCloseHorizontalWithDuration(1.0))
    }
    
}
